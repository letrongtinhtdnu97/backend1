const ProductService = require('../services/product.service')

const GetAll = async(req,res,next) => {
    try {
        const products = await ProductService.GetAll()
        return res.status(200).json({
            success: true,
            products
        })
    } catch (error) {
        return res.status(200).json({
            success: false,
            code: 99,
            products: []
        })
    }
}

const GetById = async(req,res,next) => {
    try {
        const {id} = req.body
        const product = await NewsService.GetById(id)
        return res.status(200).json({
            success: true,
            product
        })
    } catch (error) {
        return res.status(200).json({
            success: false,
            code: 99,
            product: {}
        })
    }
}
module.exports = {
    GetAll,
    GetById
}