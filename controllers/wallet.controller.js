const WalletService = require('../services/wallet.service')



const GetById = async(req,res,next) => {
    try {
        const {id} = req.body
        const wallet = await WalletService.GetById(id)
        return res.status(200).json({
            success: true,
            wallet: wallet[0]
        })
    } catch (error) {
        return res.status(200).json({
            success: false,
            code: 99,
            wallet: {}
        })
    }
}
const GetByUser = async(req,res,next) => {
    try {
        const {id} = req.body
        const wallet = await WalletService.GetByUser(id)
        return res.status(200).json({
            success: true,
            wallet: wallet[0]
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            code: 99,
            wallet: {}
        })
    }
}
const rechargeMoney = async(req,res,next) => {
    try {
        //const {adrress}
        const {id,money} = req.body;
        const sResult = await WalletService.GetById(id);
        const coint = parseFloat(money) + parseFloat(sResult[0].money)
        await WalletService.Update(id,coint);
        return res.status(200).json({
            code: 0,
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            code: 99
        })
    }
}
module.exports = {

    GetById,
    GetByUser,
    rechargeMoney
}