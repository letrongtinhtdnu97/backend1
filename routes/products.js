var express = require('express');
var router = express.Router();
const ProductController = require('../controllers/product.controller');
/* GET users listing. */
router.get('/', ProductController.GetAll);
router.post('/id',ProductController.GetById)
module.exports = router;
