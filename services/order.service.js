const pool = require('../config/db.config')
class OrderService {

    constructor() {}

    async Add(body) {
        const [rows, fields] = await 
        pool.query(`
        INSERT INTO orders ( address_wallet, products, address, payment, address_card, status) 
        VALUES (?,?,?,?,?,?);
        `,[body.address_wallet,body.products,body.address,body.payment,body.address_card,body.status])
        return rows;
    }
    
    async GetAll() {
        const [rows, fields] = await 
        pool.query('select * from orders');
        return rows;
    }
    async GetById(id) {
        const [rows, fields] = await 
        pool.query('select * from orders where id = ?',[id]);
        return rows;
    }

}

module.exports = new OrderService

